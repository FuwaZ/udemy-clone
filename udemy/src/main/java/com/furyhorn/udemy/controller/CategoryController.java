package com.furyhorn.udemy.controller;

import com.furyhorn.udemy.common.product.Category;
import com.furyhorn.udemy.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/categories")
public class CategoryController {
    private CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping(path = {"","/"})
    private List<Category> getCategoryList(){
         return categoryService.findAll();
     }

    @GetMapping("/limit/{number}")
    private List<Category> getCategoryListLimitByNumber(@PathVariable("number") Long number){
        return categoryService.findAll();
    }
}
