package com.furyhorn.udemy.common.product;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SubCategory{
    private long id;
    private String name;
    private Category category;
}
