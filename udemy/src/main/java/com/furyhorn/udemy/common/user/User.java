package com.furyhorn.udemy.common.user;


import com.furyhorn.udemy.common.product.Course;

import javax.persistence.*;
import java.util.Set;

@MappedSuperclass
public abstract class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String email;
}
