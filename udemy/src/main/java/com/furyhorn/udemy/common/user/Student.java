package com.furyhorn.udemy.common.user;

import com.furyhorn.udemy.common.product.Course;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Student extends User{
    @ManyToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(name = "course_student",
            joinColumns = @JoinColumn(name = "student_id"),
            inverseJoinColumns = @JoinColumn(name = "course_id"))
    private Set<Course> courses;
}
