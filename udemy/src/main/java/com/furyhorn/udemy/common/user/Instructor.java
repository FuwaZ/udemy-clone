package com.furyhorn.udemy.common.user;

import com.furyhorn.udemy.common.product.Course;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
public class Instructor extends User {
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "course_instructor",
            joinColumns = @JoinColumn(name = "instructor_id"),
            inverseJoinColumns = @JoinColumn(name = "course_id"))
    private Set<Course> courses;
}
