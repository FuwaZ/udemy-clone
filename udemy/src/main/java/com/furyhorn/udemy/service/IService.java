package com.furyhorn.udemy.service;

import java.util.List;

/**
 *
 * @param <T1> type of the object
 * @param <T2> type of the id of the object
 */
public interface IService<T1,T2> {
    public void save(T1 obj);
    public T1 findById(T2 id);
    public List<T1> findAll();
    public void deleteById(T2 id);
    public void deleteAll();
}
