package com.furyhorn.udemy.service;

import com.furyhorn.udemy.common.user.Instructor;
import com.furyhorn.udemy.repository.InstructorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InstructorService implements IService<Instructor, Long> {
    private InstructorRepository instructorRepository;

    @Autowired
    public InstructorService(InstructorRepository instructorRepository) {
        this.instructorRepository = instructorRepository;
    }

    @Override
    public void save(Instructor obj) {

    }

    @Override
    public Instructor findById(Long id) {
        return null;
    }

    @Override
    public List<Instructor> findAll() {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public void deleteAll() {

    }
}
