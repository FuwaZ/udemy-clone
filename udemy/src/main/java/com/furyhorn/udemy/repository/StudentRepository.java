package com.furyhorn.udemy.repository;

import com.furyhorn.udemy.common.user.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
