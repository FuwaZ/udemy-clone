package com.furyhorn.udemy.repository;

import com.furyhorn.udemy.common.user.Instructor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InstructorRepository extends JpaRepository<Instructor, Long> {
}
