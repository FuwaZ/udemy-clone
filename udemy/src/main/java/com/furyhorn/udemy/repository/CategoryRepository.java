package com.furyhorn.udemy.repository;

import com.furyhorn.udemy.common.product.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {
}
