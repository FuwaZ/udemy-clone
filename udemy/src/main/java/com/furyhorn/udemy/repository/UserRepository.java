package com.furyhorn.udemy.repository;

import com.furyhorn.udemy.common.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
