package com.furyhorn.udemy.repository;

import com.furyhorn.udemy.common.product.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course, Long> {
}
